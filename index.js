// console.log("Hello!");

// [SECTION] Exponent Operator

const firstNum = 8 ** 2;
console.log(firstNum);

const secondNum = Math.pow(8, 2);
console.log(secondNum);

// [SECTION] Template Literals
/*
    - Allows to write strings without using the concatenation operator (+)
    - Greatly helps with code readability
*/

let name = "John";

// This is without template literals
let message = "Hello " + name + "! Welcome to programming!";
console.log("Message without template literals:\n" + message);

// String using template literals
// Uses backticks (``)

message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals:\n${message}`);


const anotherMessage = `${name} attended a math competition.\nHe won it by solving the problem 8**2 with the solution of ${firstNum}`;
console.log(anotherMessage);

/*
    - Template literals allows us to write strings with embedded Javascript expressions
    - expressions are any valid unit of code that resolves to a value
    - "${}" are used to include Javascript expressions in strings using template literals
*/

const interestRate = .1;
const principal = 100;

console.log(`The interest of your savings account is ${principal*interestRate}.`);

// [SECTION] Array Destructuring
/*
    - Allows us to unpact elements in array into distinct variables
    - Allows us to name array elements with variables instead of using index numbers
    - Helps with code readability
    Syntax
        let/const [variableName, variableName, variableName] = array;
*/
// This is the old version
const fullName = ["Juan", "Dela", "Cruz"];

// Pre-array Destructuring
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

// This is the new version ES6 Update
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to see you`);

// [SECTION] Object Destructuring
/*
    - Allows us to unpack properties/keys of objects into distinct variables
    - Shortens the syntax for accessing properties from objects
    Syntax
        let/const {key1, key2, key3} = object;
*/

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    familyName: "Cruz"
};

console.log(person.givenName);
console.log(person.maidenName);
console.log(person.familyName);
console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's nice to see you again`);

// Object Destructuring // ES6 Update
const {givenName, maidenName, familyName} = person;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's nice to see you again`);

// [SECTION] Arrow Functions
/*
    - Compact alternative syntax to traditional functions
    - Useful for code snippets where creating function will not be reused in any other portion of the code
    - Adheres to "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name of function that will only be used in certain code snippets
    
    Syntax
        const variableName = () => {
            console.log("");
        }
*/

const hello = () => {
    console.log("Hello World");
};
hello();

const printFullName = (firstName, middleInitial, lastName) => {
    console.log(`${firstName} ${middleInitial} ${lastName}`);
};
printFullName("John", "D", "Smith");


const students = ["John", "Jane", "Judy"];

students.forEach(function(student){
    console.log(`${students} is a student.`);
})

// ES6 update
students.forEach((students) => {
    console.log(`${students} is a student.`);
})

// [SECTION] Implicit Return Statement
/*
    - There are instances when you can omit the return statement
    - This works without the return statement Javascript implicitly adds it for the result of the function
*/

// Pre-Arrow function
/*
const add = (numA, numB) => {
    return numA + numB;
}
let total = add(1,2);
console.log(total);
*/

const add = (numA, numB) => numA + numB;
let total = add(1,2);
console.log(total);

// [SECTION] Default Function Argument Value
// Provides a default argument value if none is provided

const greet = (name = "User") => {
    return `Good morning, ${name}`
}
console.log(greet("John"));
console.log(greet());

// [SECTION] Class-Based Object Blueprints
/*
    - Allows creation/instantation of objects using classes as blueprint

    Syntax
        class className{
            constructor(objectPropertyA, objectPropertyA){
                this.objectPropertyA = objectPropertyB
                this.objectPropertyB = objectPropertyB
            }
        }
*/

class Car {
    constructor(brand, name, year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Range Raptor";
myCar.year = 2021;
console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);





